package com.aman.crypto.models;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MyRecipient implements Serializable {
    private Integer pkmyrecipientid;
    private String myrecipientname;

    public MyRecipient(Integer pkMyRecipientId, String myRecipientName) {
        this.pkmyrecipientid = pkMyRecipientId;
        this.myrecipientname = myRecipientName;
    }
    @Override
    public String toString() {
        return "MyRecipient{" +
                "pkmyrecipientid=" + pkmyrecipientid +
                ", myrecipientname='" + myrecipientname + '\'' +
                '}';
    }
}
