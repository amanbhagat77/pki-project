package com.aman.crypto.models;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class MyCountry implements Serializable {
    private Integer pkMyCountryID;
    private String myCountryName;
    private String myCountryCapital;

    public MyCountry(Integer pkMyCountryID, String myCountryName, String myCountryCapital) {
        this.pkMyCountryID = pkMyCountryID;
        this.myCountryName = myCountryName;
        this.myCountryCapital = myCountryCapital;
    }

    @Override
    public String toString() {
        return "MyCountry{" +
                "pkMyCountryID=" + pkMyCountryID +
                ", myCountryName='" + myCountryName + '\'' +
                ", myCountryCapital='" + myCountryCapital + '\'' +
                '}';
    }
}
