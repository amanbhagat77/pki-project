package com.aman.crypto.models;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MyDigitalAsset implements Serializable {
    private Integer pkMyDigitalAssetId;
    private Integer fkMyCountryId;
    private String myDocType;
    private String myCountryFiles;

    public MyDigitalAsset(Integer pkMyDigitalAssetId, Integer fkMyCountryId, String myDocType, String myCountryFiles) {
        this.pkMyDigitalAssetId = pkMyDigitalAssetId;
        this.fkMyCountryId = fkMyCountryId;
        this.myDocType = myDocType;
        this.myCountryFiles = myCountryFiles;
    }

    @Override
    public String toString() {
        return "MyDigitalAsset{" +
                "pkMyDigitalAssetId=" + pkMyDigitalAssetId +
                ", fkMyCountryId=" + fkMyCountryId +
                ", myDocType='" + myDocType + '\'' +
                ", myCountryFiles='" + myCountryFiles + '\'' +
                '}';
    }
}
