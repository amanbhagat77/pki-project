package com.aman.crypto.service;

import com.aman.crypto.models.MyCountry;
import com.aman.crypto.models.MyDigitalAsset;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class LoadData {

    public static String loadJSON() throws Exception{
        File file = ResourceUtils.getFile("classpath:myrecipients.json");
        //Resource resource = new ClassPathResource("static/country-state.json");
        InputStream inputStream = new FileInputStream(file);
        InputStreamReader isReader = new InputStreamReader(inputStream);
        //InputStreamReader isReader = new InputStreamReader(resource.getInputStream());
        BufferedReader reader = new BufferedReader(isReader);
        StringBuffer sb = new StringBuffer();
        String line;

        while((line = reader.readLine()) != null) {
            sb.append(line);
        }

        String content = sb.toString();
        reader.close();

        return content;
    }

    public static List<MyDigitalAsset> loadDigitalAssets() {
        ArrayList<MyDigitalAsset> digitalAssets = new ArrayList<>();
        digitalAssets.add(new MyDigitalAsset(10001,10001, "Image", "myfiles\\images\\mycountryimage10001.jpg"));
        digitalAssets.add(new MyDigitalAsset(10002, 10001, "Audio", "myfiles\\audio\\mycountryaudio10002.mp3"));
        digitalAssets.add(new MyDigitalAsset(10003, 10001, "Video", "myfiles\\video\\mycountryvideo10003.mp4"));
        digitalAssets.add(new MyDigitalAsset(10004, 10001, "PDF", "myfiles\\docs\\mycountrydoc10004.pdf"));
        return digitalAssets;
    }

    public static List<MyCountry> loadCountries() {
        ArrayList<MyCountry> myCountries = new ArrayList<>();
        myCountries.add(new MyCountry(10001, "India", "New Delhi"));
        myCountries.add(new MyCountry(10002, "USA", "Washington DC"));
        myCountries.add(new MyCountry(10003, "Great Britain", "London"));
        myCountries.add(new MyCountry(10004, "UAE", "Abu Dhabi"));
        myCountries.add(new MyCountry(10005, "Japan", "Tokyo"));
        return myCountries;
    }
}
