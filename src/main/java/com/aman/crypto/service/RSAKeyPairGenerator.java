package com.aman.crypto.service;

import com.aman.crypto.Utils.FileCreation;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.security.*;

@Slf4j
public class RSAKeyPairGenerator {

    private PrivateKey privateKey;
    private PublicKey publicKey;

    public RSAKeyPairGenerator() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);
        KeyPair pair = keyGen.generateKeyPair();
        this.privateKey = pair.getPrivate();
        this.publicKey = pair.getPublic();
        log.info("Generating Private and Public Key Pair");
    }

    public void writeToFile(String path, byte[] key) throws IOException {
        File f = new File(path);
        f.getParentFile().mkdirs();

        FileOutputStream fos = new FileOutputStream(f);
        fos.write(key);
        fos.flush();
        fos.close();
        log.info("Writing Key to " + path + " path");
    }

    public PrivateKey getPrivateKey(){
        return privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public static void generateKey() throws NoSuchAlgorithmException, IOException {
        String publicKeyPath = FileCreation.publicKeyPath;
        String privateKeyPath = FileCreation.privateKeyPath;
        RSAKeyPairGenerator keyPairGenerator = new RSAKeyPairGenerator();
        keyPairGenerator.writeToFile(publicKeyPath, keyPairGenerator.getPublicKey().getEncoded());
        keyPairGenerator.writeToFile(privateKeyPath, keyPairGenerator.getPrivateKey().getEncoded());
        //System.out.println(Base64.getEncoder().encodeToString(keyPairGenerator.getPublicKey().getEncoded()));
        //System.out.println(Base64.getEncoder().encodeToString(keyPairGenerator.getPrivateKey().getEncoded()));
    }
}
