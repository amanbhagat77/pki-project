package com.aman.crypto.Utils;

import com.aman.crypto.models.MyCountry;
import com.aman.crypto.models.MyDigitalAsset;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import javax.sound.midi.Patch;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@PropertySource(ignoreResourceNotFound = false, value = "classpath\\Resources:application.properties")
@Slf4j
public class FileCreation {

    public static final String filePath ="C:/Users/amanb/Documents/Crypto/";
    public static final String DatFilePath ="C:/Users/amanb/Documents/Crypto/dat-files/";
    public static final String signedFilePath = filePath + "signed/";
    public static final String tempFilePath = filePath + "temp/";
    public static final String decryptedFilePath = filePath + "decrypted/";
    public static final String publicKeyPath = filePath + "RSA/publicKey.key";
    public static final String privateKeyPath = filePath + "RSA/privateKey.key";

    public static <T> void createDatFile(List<T> dataList, String fileName) throws IOException {
        System.out.println(filePath);
        File file = new File(DatFilePath+fileName + ".dat");
        System.out.println(file.getAbsolutePath());
        FileOutputStream outputStream = new FileOutputStream(file.getAbsolutePath());
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        for(T t : dataList){
            objectOutputStream.writeObject(t);
        }
        objectOutputStream.reset();
        objectOutputStream.close();
        log.info("Dat file created for: " + fileName);
    }

    public static void createFileWithExtension(byte[] dataBytes, String fileName, String filePath, String extension) throws Exception{
        File file = new File(filePath+fileName + extension);
        OutputStream outputStream = new FileOutputStream(file);
        outputStream.write(dataBytes);
        log.info("File created: " + fileName+extension + " in the file path " + filePath);
    }

    public static byte[] readBytesFromFile(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        log.info("Reading file from " + filePath);
        return Files.readAllBytes(path);
    }

    public static <T> List<T> readDatFile(String fileName) throws Exception {
        ArrayList<T> dataList = new ArrayList<>();
        File file = new File(DatFilePath+fileName + ".dat");
        System.out.println(file.getAbsolutePath());

        ObjectInputStream input = new ObjectInputStream(new FileInputStream(file.getAbsolutePath()));
        Object object = null;
        try {
            while ((object = input.readObject()) != null) {
                dataList.add((T) object);
            }
        } catch (IOException e) {

        } catch (ClassNotFoundException e) {

        }
        log.info("Reading DAT file: " + fileName);
        return dataList;
    }

}
