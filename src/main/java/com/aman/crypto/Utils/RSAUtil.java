package com.aman.crypto.Utils;

import com.aman.crypto.models.MyCountry;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class RSAUtil {

    public static PublicKey getPublicKey(String filePath) throws Exception{
        File file = new File(filePath);
        FileInputStream fis = new FileInputStream(file);
        byte[] publicKeyBytes = fis.readAllBytes();
        PublicKey publicKey = null;
        try{
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(keySpec);
            return publicKey;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        log.info("Getting public key from " +  filePath);
        return publicKey;
    }

    public static PrivateKey getPrivateKey(String filePath) throws Exception{

        File file = new File(filePath);
        FileInputStream fis = new FileInputStream(file);
        byte[] privateKeyBytes = fis.readAllBytes();

        PrivateKey privateKey = null;
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        KeyFactory keyFactory = null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            privateKey = keyFactory.generatePrivate(keySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        log.info("Getting private key from " +  filePath);
        return privateKey;
    }

//    public static byte[] encrypt(String data, String publicKey) throws Exception InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
//        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//        cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
//        return cipher.doFinal(data.getBytes());
//    }

    public static <T> byte[] encrypt(T data, PublicKey publicKey) throws Exception, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(data);
        oos.flush();
        byte [] dataBytes = bos.toByteArray();
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        // An error is reported when more than 117 bytes are encrypted. For this purpose, segmentation encryption is used to encrypt
        byte[] enBytes = null;
        for (int i = 0; i < dataBytes.length; i += 64) {
            // Note that you want to use a multiple of 2, otherwise the encrypted content will be garbled when decrypted.
            byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(dataBytes,i,i+64));
            enBytes = ArrayUtils.addAll(enBytes, doFinal);
        }
        log.info("Encripting Data with public key");
        return enBytes;
    }

    public static <T> byte[] encrypt(byte[] dataBytes, PublicKey publicKey) throws Exception, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException { ;
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        // An error is reported when more than 117 bytes are encrypted. For this purpose, segmentation encryption is used to encrypt
        byte[] enBytes = null;
        for (int i = 0; i < dataBytes.length; i += 64) {
            // Note that you want to use a multiple of 2, otherwise the encrypted content will be garbled when decrypted.
            byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(dataBytes,i,i+64));
            enBytes = ArrayUtils.addAll(enBytes, doFinal);
        }
        log.info("Encripting Data with public key");
        return enBytes;
    }

    public static <T> byte[] encryptList(List<T> dataList, PublicKey publicKey) throws Exception, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        for(T t : dataList){
            oos.writeObject(t);
        }
        oos.reset();
        oos.close();
        byte [] dataBytes = bos.toByteArray();
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        // An error is reported when more than 117 bytes are encrypted. For this purpose, segmentation encryption is used to encrypt
        byte[] enBytes = null;
        for (int i = 0; i < dataBytes.length; i += 64) {
            // Note that you want to use a multiple of 2, otherwise the encrypted content will be garbled when decrypted.
            byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(dataBytes,i,i+64));
            enBytes = ArrayUtils.addAll(enBytes, doFinal);
        }
        log.info("Encripting Data with public key");
        return enBytes;
    }

    public static Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        log.info("Deserializing bytes to object");
        return is.readObject();
    }

    public static <T> List<T> deserializeList(byte[] data) throws IOException, ClassNotFoundException {
        List<T> dataList = new ArrayList<>();
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        Object object = null;
        try {
            while ((object = is.readObject()) != null) {
                dataList.add((T) object);
            }
        } catch (IOException e) {

        } catch (ClassNotFoundException e) {

        }
        log.info("Deserializing bytes to object list");
        return dataList;
    }

    public static byte[] decrypt(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, IOException, ClassNotFoundException {
        StringBuilder sb = new StringBuilder();
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decBytes = null;
        for (int i = 0; i < data.length; i += 128) {
            byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(data, i, i + 128));
            decBytes = ArrayUtils.addAll(decBytes, doFinal);
        }
        log.info("Decrypting Data with private key");
        return decBytes;
    }

    public static <T> List<T> decryptList(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, IOException, ClassNotFoundException {
        StringBuilder sb = new StringBuilder();
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decBytes = null;
        for (int i = 0; i < data.length; i += 128) {
            byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(data, i, i + 128));
            decBytes = ArrayUtils.addAll(decBytes, doFinal);
        }
        List<T> t = deserializeList(decBytes);
        log.info("Decrypting Data with private key");
        return t;
    }

//    public static String decrypt(String data, String base64PrivateKey) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
//        return decrypt(Base64.getDecoder().decode(data.getBytes()), getPrivateKey(base64PrivateKey));
//    }

    public static <T> void startEncryptionAndDecryption(List<T> dataList) throws IllegalBlockSizeException, InvalidKeyException,
            NoSuchPaddingException, BadPaddingException, Exception {

        PublicKey publicKey = getPublicKey("RSA/publicKey.key");
        PrivateKey privateKey = getPrivateKey("RSA/privateKey.key");
        try {
            //String encryptedString = Base64.getEncoder().encodeToString(encrypt("Dhiraj is the author", publicKey));
            byte[] encryptedString = encryptList(dataList, publicKey);
            System.out.println(encryptedString);
            List<MyCountry> decryptedString = RSAUtil.decryptList(encryptedString, privateKey);
            for(MyCountry myCountry1 : decryptedString){
                System.out.println(myCountry1);
            }
        } catch (NoSuchAlgorithmException e) {
            System.err.println(e.getMessage());
        }

    }

    public static <T> void startEncryption(List<T> dataList, PublicKey publicKey, String fileName) throws Exception{
        String rootFilePath = FileCreation.filePath;
        String signedFilePath = rootFilePath+"\\signed\\";
        byte[] encryptedString = encryptList(dataList, publicKey);
        FileCreation.createFileWithExtension(encryptedString, fileName,signedFilePath, ".signed");
    }

    public static <T> List<T> startDecrypting(byte[] dataList, PrivateKey privateKey) throws Exception{
        return RSAUtil.decryptList(dataList, privateKey);
    }
}
