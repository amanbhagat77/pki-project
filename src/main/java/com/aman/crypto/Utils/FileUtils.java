package com.aman.crypto.Utils;

import com.aman.crypto.models.MyDigitalAsset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.List;
@Configuration
@PropertySource(ignoreResourceNotFound = false, value = "classpath:application.properties")
public class FileUtils {

    public static String getFileName(String fileURL){
        return fileURL.split("\\\\")[2];
    }

    public static List<String> getFilesURL(List<MyDigitalAsset> digitalAssets){
        List<String> urlList = new ArrayList<>();
        for (MyDigitalAsset myDigitalAsset : digitalAssets){
            urlList.add(myDigitalAsset.getMyCountryFiles());
        }
        return urlList;
    }
}
