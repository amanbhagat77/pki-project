package com.aman.crypto;

import com.aman.crypto.Utils.*;
import com.aman.crypto.models.MyCountry;
import com.aman.crypto.models.MyDigitalAsset;
import com.aman.crypto.models.MyRecipient;
import com.aman.crypto.models.MyRecipientDataProvider;
import com.aman.crypto.service.LoadData;
import com.aman.crypto.service.RSAKeyPairGenerator;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipOutputStream;

@Slf4j
@SpringBootApplication
public class CryptoApplication {

    public static String publicKeyPath = FileCreation.publicKeyPath;
    public static String privateKeyPath = FileCreation.privateKeyPath;
    public static String dirPath = "C:/Users/amanb/Documents/Crypto/signed";

    public static void main(String[] args) throws Exception {
        SpringApplication.run(CryptoApplication.class, args);
        List<MyDigitalAsset> myDigitalAssetList = LoadData.loadDigitalAssets();
        List<String> fileURLs = FileUtils.getFilesURL(myDigitalAssetList);
        FileUtils utils = new FileUtils();
        RSAKeyPairGenerator.generateKey();

        //Encrypting
        log.info("====Encryption Started====");
        startEncryptingMyCountries();
        startEncryptingMyRecipients();
        startEncryptingMyDigitalAssets();
        startEncryptingFiles(fileURLs);
        log.info("====Encryption Finished====");

        //Zipping
        File file = new File(dirPath);
        Path sourceDir = Paths.get(file.getAbsolutePath());
        String timeStamp = DateTimeUtils.getTimeStamp("ddMMyyyy");
        String zipFileName = dirPath.concat("_" + timeStamp + "_" + "aman.zip");
        try{
            ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFileName));
            Files.walkFileTree(sourceDir, new ZipDir(sourceDir, zos));
            zos.close();
        }
        catch (IOException e){
            System.err.println("I/O Error: " + e);
        }

        //UnZipping





        //Decrypting
        log.info("====Decryption Started====");
        startDecryptingMyCountries();
        startDecryptingMyRecipients();
        startDecryptingMyDigitalAssets();
        startDecryptingFiles(fileURLs);
        log.info("====Decryption Finished====");

    }

    public static void startEncryptingMyCountries() throws Exception {
        List<MyCountry> myCountries = LoadData.loadCountries();
        FileCreation.createDatFile(myCountries, "myCountries");
        List<MyCountry> myCountryList = FileCreation.readDatFile("myCountries");
        PublicKey publicKey = RSAUtil.getPublicKey(publicKeyPath);
        RSAUtil.startEncryption(myCountryList, publicKey, "mycountries");
    }

    public static void startDecryptingMyCountries() throws Exception {
        String signedFilePath = FileCreation.tempFilePath;
        PrivateKey privateKey = RSAUtil.getPrivateKey(privateKeyPath);
        byte[] encryptedData = FileCreation.readBytesFromFile(signedFilePath + "mycountries.signed");
        List<MyCountry> myCountries1 = RSAUtil.startDecrypting(encryptedData, privateKey);
        for (MyCountry myCountry : myCountries1) {
            System.out.println(myCountry);
        }
    }

    public static void startEncryptingMyDigitalAssets() throws Exception {
        List<MyDigitalAsset> myDigitalAssets = LoadData.loadDigitalAssets();
        FileCreation.createDatFile(myDigitalAssets, "mydigitalasset");
        List<MyDigitalAsset> myDigitalAssetsDat = FileCreation.readDatFile("mydigitalasset");
        PublicKey publicKey = RSAUtil.getPublicKey(publicKeyPath);
        RSAUtil.startEncryption(myDigitalAssetsDat,publicKey,"mydigitalasset");
    }

    public static void startDecryptingMyDigitalAssets() throws Exception {
        String signedFilePath = FileCreation.tempFilePath;
        PrivateKey privateKey = RSAUtil.getPrivateKey(privateKeyPath);
        byte[] encryptedData = FileCreation.readBytesFromFile(signedFilePath + "mydigitalasset.signed");
        List<MyDigitalAsset> myDigitalAssetListDecrypted = RSAUtil.startDecrypting(encryptedData, privateKey);
        for (MyDigitalAsset digitalAsset : myDigitalAssetListDecrypted) {
            System.out.println(digitalAsset);
        }
    }

    public static void startEncryptingMyRecipients() throws Exception {
        String content = LoadData.loadJSON();
        Gson gson = new Gson();
        MyRecipientDataProvider myRecipientDataProvider = gson.fromJson(content, MyRecipientDataProvider.class);
        FileCreation.createDatFile(myRecipientDataProvider.getMyrecipients(), "myrecipients");
        List<MyRecipient> myRecipientsDat = FileCreation.readDatFile("myrecipients");
        PublicKey publicKey = RSAUtil.getPublicKey(publicKeyPath);
        RSAUtil.startEncryption(myRecipientsDat,publicKey,"myrecipients");
    }

    public static void startDecryptingMyRecipients() throws Exception {
        String signedFilePath = FileCreation.tempFilePath;
        PrivateKey privateKey = RSAUtil.getPrivateKey(privateKeyPath);
        byte[] encryptedData = FileCreation.readBytesFromFile(signedFilePath + "myrecipients.signed");
        List<MyRecipient> myDigitalAssetListDecrypted = RSAUtil.startDecrypting(encryptedData, privateKey);
        for (MyRecipient myRecipient : myDigitalAssetListDecrypted) {
            System.out.println(myRecipient);
        }
    }

    public static void startEncryptingFiles(List<String> fileUrls) throws Exception {
        String signedFilePath = FileCreation.signedFilePath;
        String rootFilePath = FileCreation.filePath;
        PublicKey publicKey = RSAUtil.getPublicKey(publicKeyPath);
        for(String url : fileUrls){
            String name = FileUtils.getFileName(url);
            String nameWithoutExtension = name.substring(0,name.lastIndexOf("."));
            byte[] fileBytes = FileCreation.readBytesFromFile(rootFilePath+url);
            byte[] encryptedData = RSAUtil.encrypt(fileBytes,publicKey);
            FileCreation.createFileWithExtension(encryptedData,nameWithoutExtension,signedFilePath,".signed");
        }
    }

    public static void startDecryptingFiles(List<String> fileUrls) throws Exception {
        String signedFilePath = FileCreation.tempFilePath;
        PrivateKey privateKey = RSAUtil.getPrivateKey(privateKeyPath);
        for(String url : fileUrls){
            String name = FileUtils.getFileName(url);
            String nameWithoutExtension = name.substring(0,name.lastIndexOf("."));
            String decryptedDataPath = FileCreation.decryptedFilePath;
            String extension = name.split("\\.")[1];
            byte[] encryptedDataFromSigned = FileCreation.readBytesFromFile(signedFilePath + nameWithoutExtension+".signed");
            byte[] decryptedData = RSAUtil.decrypt(encryptedDataFromSigned, privateKey);
            FileCreation.createFileWithExtension(decryptedData,nameWithoutExtension,decryptedDataPath,"."+extension);
        }
    }



}
